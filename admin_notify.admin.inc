<?php

/**
 * Implementation of hook_settings_form().
 */
function admin_notify_settings_form($form) {
  module_load_include('inc', 'admin_notify');
  $form = array();

  $form['admin_notify_content_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select the content types'),
    '#description' => t('Chose the content types on whose posting (insert/delete) the admin must be updated'),
  );

  $form['admin_notify_content_types']['admin_notify_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#default_value' => variable_get('admin_notify_node_types', array()),
    '#options' => node_type_get_names(),
  );

  $admin_mail = variable_get('site_mail', ini_get('sendmail_from'));

  $form['admin_notify_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email to whom the notification is to be sent'),
    '#default_value' => variable_get('admin_notify_email', $admin_mail),
  );

  $form['bulk_create'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email Settings'),
  );

  $form['bulk_create']['directmail_email_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Configurable email subject'),
    '#default_value' => variable_get('directmail_email_subject', 'New Content Posted'),
    '#description' => t('Enter a default subject of the notification email.'),
  );

  $form['bulk_create']['directmail_email_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Configurable email body'),
    '#default_value' => variable_get('directmail_email_body', 'A new content has been posted on your site'),
    '#description' => t('Enter the default email template to notify users about new content posted on the site. Use the following tokens: %user_who_posted, %new_content_link'),
  );

  return system_settings_form($form);
}
